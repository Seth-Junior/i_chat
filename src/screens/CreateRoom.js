import { Box, Button, Center, Heading, Input, Stack } from 'native-base'
import React from 'react'
import { useState } from 'react'
import firestore from '@react-native-firebase/firestore';


export const createChatRoom = (roomName, cb) => {
    firestore()
        .collection('MESSAGE_THREADS')
        .add({
            name: roomName,
            latestMessage: {
                text: `${roomName} created. Welcome!`,
                createdAt: new Date().getTime()
            }
        })
        .then(docRef => {
            docRef.collection('MESSAGES').add({
                text: `${roomName} created. Welcome!`,
                createdAt: new Date().getTime(),
                system: true
            })
            cb()
        })
}

const CreateRoom = ({ navigation }) => {

    const [roomName, setRoomName] = useState()

    const onCreateRoom = () => {
        createChatRoom(roomName, () => {
            navigation.navigate("Room")
        })
    }

    return (
        <Box flex={1} justifyContent="center" >
            <Stack width='100%' padding={5}>
                <Center><Heading>Create Room</Heading></Center>
                <Input placeholder="Room Name" my={5} _focus={{ borderColor: 'blue.800' }} onChangeText={setRoomName} />
                <Button backgroundColor="blue.800" onPress={onCreateRoom}>Create Room</Button>
           </Stack>
        </Box>
    )
}

export default CreateRoom
