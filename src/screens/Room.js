import React, { useState, useEffect } from 'react'
import { View, TouchableOpacity, StyleSheet } from 'react-native'
import firestore from '@react-native-firebase/firestore';
import { FlatList, Spinner, Box, HStack, VStack, Text ,Avatar } from 'native-base';


const Room = ({ navigation }) => {

    const [threads, setThreads] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        const unsubscribe = firestore()
            .collection('MESSAGE_THREADS')
            .orderBy('latestMessage.createdAt', 'desc')
            .onSnapshot(querySnapshot => {
                const threads = querySnapshot.docs.map(documentSnapshot => {
                    return {
                        _id: documentSnapshot.id,
                        name: '',
                        latestMessage: { text: '' },
                        ...documentSnapshot.data()
                    }
                })
                setThreads(threads)
                // console.log(threads)
                if (loading) {
                    setLoading(false)
                }
            })
        return () => unsubscribe()
    }, [])

    if (loading) {
        return <View style={{marginTop:20}}><Spinner size="lg"/></View>
    }

    return (
        <View>
            <FlatList
                data={threads}
                keyExtractor={item => item._id}
                renderItem={({ item }) => (
                    <TouchableOpacity onPress={() => navigation.navigate('ChatScreen', { thread: item })}>
                        <Box style={styles.boxStyle}>
                            <HStack space={3} justifyContent="space-between" alignItems="center" >
                                <Avatar
                                    size="md"
                                    source={{
                                        uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS0_vEp9SSkqa4yQRY2mzlC-PNLnzdELWCl6Q&usqp=CAU"
                                    }}
                                    mx={2}
                                />
                                <VStack width="90%">
                                    <Text style={styles.teamName}>{item.name}</Text>
                                    <Text style={styles.messageStyle}> {item.latestMessage.text} </Text>
                                </VStack>
                            </HStack>
                        </Box>
                    </TouchableOpacity>
                )}
            />
        </View>
    )
}

export default Room

const styles = StyleSheet.create({
    boxStyle: {
        borderBottomWidth:1,
        borderBottomColor:'gray',
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius:15
    },
    teamName: {
        color: "Gray.800",
        fontSize: 17,
        fontWeight: 'bold'
    },
    messageStyle: {
        color: "Gray.600"
    }
})