import { Box, Button, Center, FormControl, Heading, Input, VStack } from 'native-base'
import React, { useContext, useState } from 'react'
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { AuthContext } from '../provider/AuthProvider';
import { ScrollView, StyleSheet } from 'react-native';

const Login = ({ navigation }) => {

    var { setAuth } = useContext(AuthContext)

    const [email, setEmail] = useState('-')
    const [password, setPassword] = useState('-')

    const logIn = async () => {

        auth()
            .signInWithEmailAndPassword(email, password)
            .then((userCredential) => {
                const uid = userCredential.user.uid;
                firestore().collection('Users').doc(uid).get().then((response) => {
                    console.log("user:", response._data)
                    setAuth({
                        isAuth: true,
                        payload: response._data
                    })
                    alert('Welcome ' + response._data.username)
                }).catch(err => {
                      if (err.code === 'auth/invalid-email') {
                        alert('That email address is invalid !');
                      }
                })
            })
    }

    return (
        <ScrollView>
            <Box style={styles.boxStyle}>
                <VStack flex={1} px={5} >
                    <VStack>
                        <Center>
                            <Heading fontSize={50} mt={10}> Login </Heading>
                        </Center>
                        <VStack my={10}>
                            <FormControl>
                                <FormControl.Label _text={styles.labelStyle}> Email </FormControl.Label>
                                <Input
                                    placeholder="Input your email"
                                    autoFocus
                                    _focus={styles.focusStyle}
                                    onChangeText={setEmail}
                                />
                            </FormControl>
                            <FormControl my={5}>
                                <FormControl.Label _text={styles.labelStyle}> Password </FormControl.Label>
                                <Input
                                    placeholder="Input your passwords"
                                    _focus={styles.focusStyle}
                                    type="password"
                                    onChangeText={setPassword}
                                />
                            </FormControl>
                            <Button my={4} backgroundColor="blue.800" onPress={logIn}> Login </Button>
                            <Button backgroundColor="blueGray.500" onPress={() => navigation.push("Register")} > Register </Button>
                        </VStack>
                    </VStack>
                </VStack>
            </Box>
        </ScrollView>
    )
}

export default Login

const styles = StyleSheet.create({
    boxStyle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center"
    },
    labelStyle: {
        color: 'muted.700',
        fontSize: 'md',
        fontWeight: 700
    },
    focusStyle: {
        borderColor: 'blue.800'
    }
})