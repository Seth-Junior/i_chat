import React, { useState } from 'react'
import { Box, FormControl, Input, Button, Heading, VStack , Center } from 'native-base'
import {  ScrollView, StyleSheet } from 'react-native'
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

const RegisterScreen = ({ navigation }) => {

    const [email, setEmail] = useState('-')
    const [username, setUsername] = useState('-')
    const [password, setPassword] = useState('-')

    const onSignUp = () => {
        auth()
        .createUserWithEmailAndPassword(email, password)
        .then((res) => { 
            let uid = res.user.uid
            firestore()
            .collection('Users')
            .doc(uid)
            .set({
                uid,
                email,
                username,
            })
            alert('Created Success !')
            navigation.pop()
        })
        .catch(err => {
            if (err.code === 'auth/email-already-in-use') {
                alert('That email address is already in use !');
              }
              if (err.code === 'auth/invalid-email') {
                alert('That email address is invalid !');
              }
              if (error.code === 'auth/weak-password') {
                alert('Weak password !');
              }      
            });
    }

    return (
        <ScrollView>
            <Box style={styles.boxStyle} >
            <VStack flex={1} px={5} >
                <VStack>
                    <Center> <Heading fontSize={50}> Register </Heading></Center>

                    <VStack my={5}>
                        <FormControl>
                            <FormControl.Label _text={styles.labelStyle}> Email </FormControl.Label>
                            <Input placeholder="Email ( abc12@gmail.com )" autoFocus _focus={styles.focusStyle} onChangeText={setEmail}/>
                        </FormControl>
                        <FormControl my={5}>
                            <FormControl.Label _text={styles.labelStyle}> Username </FormControl.Label>
                            <Input placeholder="Username" _focus={styles.focusStyle} onChangeText={setUsername}/>
                        </FormControl>
                        <FormControl >
                            <FormControl.Label _text={styles.labelStyle}> Password </FormControl.Label>
                            <Input placeholder="Password ( Must be over then 5 charecters )" type="password" _focus={styles.focusStyle} onChangeText={setPassword}/>
                        </FormControl>
                        <Button mt={4} backgroundColor="blueGray.500" onPress={onSignUp}> Register </Button>
                    </VStack>
                </VStack>
            </VStack>
        </Box>
        </ScrollView>
    )
}

export default RegisterScreen

const styles = StyleSheet.create({
    boxStyle:{
        flex:1,
        flexDirection:"row",
        alignItems:"center"
    },
    labelStyle: {
        color: 'muted.700',
        fontSize: 'md',
        fontWeight: 700
    },
    focusStyle: {
        borderColor: 'blue.800'
    }
})