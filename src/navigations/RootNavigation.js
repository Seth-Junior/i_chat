import React , { useContext } from 'react'
import { NavigationContainer } from "@react-navigation/native"
import { AuthContext } from "../provider/AuthProvider"
import AuthStack from "./AuthStack"
import HomeStack from "./HomeStack"

export default function () {

    let { auth } = useContext(AuthContext)
  
    return (
      <NavigationContainer>
        {auth.isAuth ? <HomeStack /> : <AuthStack />}
      </NavigationContainer>
    )
  }