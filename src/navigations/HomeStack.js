import React , {useContext} from 'react'
import { AuthContext } from '../provider/AuthProvider'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Entypo from 'react-native-vector-icons/Entypo'
import { Avatar, Box, IconButton } from 'native-base'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Room from '../screens/Room';
import ChatScreen from '../screens/ChatScreen';
import CreateRoom from '../screens/CreateRoom';

const Stack = createNativeStackNavigator();

const HomeStack = () => {

    let { auth, setAuth } = useContext(AuthContext)

    return (
        <Stack.Navigator>
            <Stack.Screen name="Room" component={Room}
                options={({ navigation }) => ({
                    title: 'All Rooms',
                    headerTitleAlign: 'center',
                    headerLeft: () => (
                        <IconButton
                            size='sm'
                            _icon={{
                                as: Entypo,
                                name: "chevron-with-circle-left",
                            }}
                            onPress={() => setAuth({ auth: false, payload: null })}
                        />
                    ),
                    headerRight: () => (
                        <>
                            <Avatar
                                size="sm"
                                source={{
                                    uri: "https://ui-avatars.com/api/?name=" + auth.payload.username,
                                }}
                                mx={1}
                            />
                            <Box>
                                <IconButton
                                    size="md"
                                    _icon={{
                                        as: Ionicons,
                                        name: "add-circle-outline",
                                    }}
                                    onPress={() => navigation.navigate("CreateRoom")}
                                />
                            </Box>
                        </>
                    ),
                })}
            />
            <Stack.Screen
                name="ChatScreen"
                component={ChatScreen}
                options={({ route }) => ({
                    title: route.params.thread.name,
                    headerTitleAlign:'center'
                })}
            />
            <Stack.Screen name="CreateRoom" component={CreateRoom} options={{ title: 'Create Room', headerTitleAlign: 'center' }} />
        </Stack.Navigator>
    )
}

export default HomeStack
