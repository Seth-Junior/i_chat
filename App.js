import React from 'react'
import { NativeBaseProvider } from 'native-base';
import AuthProvider from './src/provider/AuthProvider';
import Root from './src/navigations/RootNavigation';

const App = () => {
  return (
    <NativeBaseProvider>
        <AuthProvider>
           <Root />
        </AuthProvider>
    </NativeBaseProvider>
  )
}



export default App
